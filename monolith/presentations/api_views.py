from django.http import JsonResponse

from .models import Presentation, Status
from common.json import ModelEncoder
from events.models import Conference
from events.api_views import ConferenceListEncoder

from django.views.decorators.http import require_http_methods
import json


# class PresentationListEncoder(ModelEncoder):
#     model = Presentation
#     properties = [
#         "presenter_name",
#         "company_name",
#         "presenter_email",
#         "title",
#         "synopsis",
#         "created",
#     ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    encoders = {
        "conference": ConferenceListEncoder,
    }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):

    if request.method == "GET":
        presentation = Presentation.objects.filter(conference=conference_id)

        return JsonResponse(
            {"presentation": presentation},
            encoder=PresentationListEncoder,
        )

    else:
        content = json.loads(request.body)
        # try:
        content["conference"] = Conference.objects.get(id=conference_id)
        # content["status"] = Status.objects.get(name="SUBMITTED")
        # except Conference.DoesNotExit:
        # return JsonResponse(
        #     {"message": "Invalid location id"},
        #     status=400,
        # )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
