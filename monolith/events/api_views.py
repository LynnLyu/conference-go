from django.http import JsonResponse

from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    # response = []
    # conferences = Conference.objects.all()
    # for conference in conferences:
    #     response.append(
    #         {
    #             "name": conference.name,
    #             "href": conference.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"conferences": response})
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


# class TryConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     properties = [
#         "name",
#         "description",
#         "max_presentations",
#         # "max_attendees",
#         # "starts",
#         # "ends",
#         # "created",
#         # "updated",

#     ]


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
        # "get_weather",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


# class TryConferenceDetailEncoder(ModelEncoder):
#     model = Conference
#     properties = [
#         "name",
#         "description",
#         "max_presentations",
#         "max_attendees",
#         # "starts",
#         # "ends",
#         # "created",
#         # "updated",
#     ]


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):

    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        get_weather = get_weather_data(
            conference.location.city, conference.location.state
        )
        # conference.update(get_weather)

        return JsonResponse(
            {"conference": conference, "weather": get_weather},
            ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**conference)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            {
                "conference": conference,
            },
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):

    # location = Location.objects.all()
    # response = []
    # for l in location:
    #     response.append({"name": l.name, "href": l.get_api_url()})
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        # print(content)
        get_a_photo = get_photo(content["city"], content["state"].abbreviation)

        content.update(get_a_photo)

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
        # get_photo = get_a_photo(content["city"], content["state"].abbreviation)
        # content.update(get_a_photo)


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        # "state",
        "room_count",
        "created",
        "updated",
        "photo",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):

    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
